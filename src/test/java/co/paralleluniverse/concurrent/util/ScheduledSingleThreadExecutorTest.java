/*
 * Quasar: lightweight threads and actors for the JVM.
 * Copyright (c) 2013-2014, Parallel Universe Software Co. All rights reserved.
 * 
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *  
 *   or (per the licensee's choosing)
 *  
 * under the terms of the GNU Lesser General Public License version 3.0
 * as published by the Free Software Foundation.
 */
package co.paralleluniverse.concurrent.util;

import co.paralleluniverse.common.test.TestUtil;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TestName;
import org.junit.rules.TestRule;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

/**
 *
 * @author pron
 */
public class ScheduledSingleThreadExecutorTest {
    @Rule
    public TestName name = new TestName();
    @Rule
    public TestRule watchman = TestUtil.WATCHMAN;

    private ScheduledSingleThreadExecutor exec;

    public ScheduledSingleThreadExecutorTest() {
    }

    @Before
    public void setUp() {
        exec = new ScheduledSingleThreadExecutor();
    }

    @After
    public void tearDown() {
        exec.shutdown();
    }

    @Test
    public void testFixedRate() throws Exception {
        final AtomicInteger counter = new AtomicInteger();

        exec.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                counter.incrementAndGet();
            }
        }, 0, 30, TimeUnit.MILLISECONDS);

        Thread.sleep(200);

        final int count = counter.get();
        assertTrue("count: " + count, count > 5 && count < 9);
    }


    @Test
    public void testMultipleProducers() throws Exception {
        final int nThread = Runtime.getRuntime().availableProcessors();
        final int nSchedules = 5000;

        final AtomicInteger counter2 = new AtomicInteger();
        final AtomicInteger counter = new AtomicInteger();


        final Runnable command = counter::incrementAndGet;

        for (int t = 0; t < nThread; t++) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        AtomicInteger c2 = counter2;
                        ScheduledSingleThreadExecutor exec = ScheduledSingleThreadExecutorTest.this.exec;

                        for (int i = 0; i < nSchedules; i++) {
                            c2.incrementAndGet();
                            exec.schedule(command, 0, TimeUnit.MILLISECONDS);
                        }
                    } catch (Throwable t) {
                        t.printStackTrace();
                    }
                }
            }).start();
        }

        Thread.sleep(200);

        assertThat(counter2.get(), is(nThread * nSchedules));
        assertThat(counter.get(), is(nThread * nSchedules));
    }

}
