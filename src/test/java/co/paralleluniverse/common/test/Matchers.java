/*
 * Copyright (c) 2013-2014, Parallel Universe Software Co. All rights reserved.
 * 
 * This program and the accompanying materials are dual-licensed under
 * either the terms of the Eclipse Public License v1.0 as published by
 * the Eclipse Foundation
 *  
 *   or (per the licensee's choosing)
 *  
 * under the terms of the GNU Lesser General Public License version 3.0
 * as published by the Free Software Foundation.
 */
package co.paralleluniverse.common.test;

import org.mockito.ArgumentMatcher;
import org.mockito.internal.matchers.*;

/**
 *
 * @author pron
 */
public final class Matchers {

    // Comparable

    public static <T extends Comparable<T>> ArgumentMatcher<T> lessThan(Comparable<T> value) {
        return new LessThan<T>(value);
    }

    public static <T extends Comparable<T>> ArgumentMatcher<T> lessOrEqual(Comparable<T> value) {
        return new LessOrEqual<T>(value);
    }

    public static <T extends Comparable<T>> ArgumentMatcher<T> greaterThan(Comparable<T> value) {
        return new GreaterThan<T>(value);
    }

    public static <T extends Comparable<T>> ArgumentMatcher<T> greaterOrEqual(Comparable<T> value) {
        return new GreaterOrEqual<T>(value);
    }

    // Number
    public static ArgumentMatcher<Number> equalsWithDelta(Number value, Number delta) {
        return new EqualsWithDelta(value, delta);
    }
    
    // String

    public static ArgumentMatcher<String> startsWith(String prefix) {
        return new StartsWith(prefix);
    }

    public static ArgumentMatcher<String> endsWith(String suffix) {
        return new EndsWith(suffix);
    }

    public static ArgumentMatcher<String> contains(String string) {
        return new Contains(string);
    }

    public static ArgumentMatcher<String> matches(String regex) {
        return (ArgumentMatcher<String>)((Object)new Matches(regex));
    }

    public static ArgumentMatcher<String> find(String regex) {
        return new Find(regex);
    }

    // Object

    public static ArgumentMatcher<Object> instanceOf(Class clazz) {
        return new InstanceOf(clazz);
    }

    // 
    private Matchers() {
    }
}
